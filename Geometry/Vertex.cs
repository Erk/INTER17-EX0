﻿using System;
namespace Geometry
{
    public struct Vertex
    {
        public int x, y;
        public Vertex (int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public override string ToString ()
        {
            return string.Format ("({0}; {1})", x, y);
        }
        public static bool operator == (Vertex a, Vertex b)
        {
            return ((a.x == b.x) && (a.y == b.y));
        }
        public static bool operator != (Vertex a, Vertex b)
        {
            return !(a == b);
        }
        public override bool Equals (object obj)
        {
            if (obj is Vertex) {
                return this == obj as Vertex?;
            } else {
                return false;
            }
        }
        public override int GetHashCode ()
        {
            return (x * y / 2 * y * x * x * y + 2 * x * y + y);
        }

    }
}
