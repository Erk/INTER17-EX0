﻿using System;

namespace TUI
{
    class MainClass
    {
        public static int Main (string [] args)
        {
            var tester = new Tester ();
            var test1 = new Geometry.Vertex (3, 4);
            var test2 = new Geometry.Vertex (4, 3);
            var test3 = new Geometry.Vertex (3, 4);

            tester.Test ((test1.ToString () == "(3; 4)"), true, "Vertexen test1 bliver printet som (3; 4)");
            tester.Test ((test1 == test3), true, "Vertexen test1 er lig vertexen test3 (==)");
            tester.Test ((test1 == test2), false, "Vertexen test1 er ikke lig vertexen test2 (==)");
            tester.Test ((test1 != test3), false, "Vertexen test1 er lig vertexen test3 (!=)");
            tester.Test ((test1 != test2), true, "Vertexen test1 er ikke lig vertexen test2 (!=)");
            //v.Equals(w) ==> (v.GetHashCode() == w.GetHashCode())
            tester.Test ((test1.Equals (test3)), true, "test1.Equals(test2)");
            tester.Test ((test1.Equals (test2)), false, "test1.Equals(test2)");
            Console.Error.WriteLine ("test1.GetHashCode(): {0}", (test1.GetHashCode()));
            Console.Error.WriteLine ("test2.GetHashCode(): {0}", (test2.GetHashCode()));
            Console.Error.WriteLine ("test3.GetHashCode(): {0}", (test3.GetHashCode()));
            tester.Test ((test1.GetHashCode () == test3.GetHashCode ()), true, "test1.GetHashCode() == test3.GetHashCode()");
            tester.Test ((test1.GetHashCode () != test2.GetHashCode ()), true, "test1.GetHashCode() == test2.GetHashCode()");
            if (args.Length > 0) {
                if (args [0] == "fail") {
                    tester.Test ((test1.GetHashCode () != test3.GetHashCode ()), true, "test1.GetHashCode() != test3.GetHashCode()");
                }
            }
            Console.WriteLine (tester.State);
            if (tester.State == Tester.StateType.Successful) {
                return 0;
            } else {
                return 1;
            }
        }
    }
    class Tester
    {
        public enum StateType { Successful, Failed };
        public StateType State = StateType.Successful;

        public void Test (object actual, object expected, string messege)
        {
            if (actual.Equals (expected) == false){
                State = StateType.Failed;
                Console.Error.WriteLine (messege);
            }
        }
    }
}
